import os
from setuptools import setup, find_packages


VERSION = "0.2"

setup(
    name = "fm.utils",
    version = VERSION,
    author="Julian Bez",
    author_email="julian@freshmilk.tv",
    url="http://www.freshmilk.tv",
    description = """Freshmilk Utils App""",
    packages=find_packages(),
    namespace_packages = [],
    include_package_data = True,
    zip_safe=False,
    license="BSD",
    install_requires=["requests", "Pillow"]
)
